<?php

use app\models\User;

return [
    'debug'                  => 1,
    'password.salt'          => 'wI#JHm!!V1h!mHO%',
    'identity.model'         => User::class,
    'main.defaultController' => 'task',
    'main.defaultAction'     => 'index',
    'main.nameProject'       => 'TaskBook',
    'database'               => [
        'dsn'      => 'mysql:host=localhost;dbname=beejee',
        'username' => 'root',
        'password' => 'root',
        'charset'  => 'utf8',
    ],
];
