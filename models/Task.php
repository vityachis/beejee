<?php

namespace app\models;

use app\core\classes\DBWorker;

/**
 * Class Task
 *
 * @property int|null $id
 * @property int|null $user_id
 * @property string|null $username
 * @property string|null $email
 * @property string $text
 * @property int $status
 * @property int $update_user_id
 */
class Task extends DBWorker
{
    public const COUNT_ON_PAGE = 3;

    public const STATUS_NEW       = 1;
    public const STATUS_COMPLETED = 2;

    /**
     * @var string[]
     */
    public static $statuses = [
        self::STATUS_NEW       => 'New',
        self::STATUS_COMPLETED => 'Completed',
    ];

    /**
     * Task constructor.
     *
     * @param string $nameTable
     * @param bool $initFieldNames
     */
    public function __construct(string $nameTable = 'task', bool $initFieldNames = false)
    {
        parent::__construct($nameTable, $initFieldNames);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return self::$statuses[$this->status] ?? 'Unknown';
    }

    /**
     * @return string
     */
    public function getUpdatedBy(): string
    {
        $user = (new User())->findOne('id', $this->update_user_id);

        return $user->username ?? '(Administrator)';
    }
}
