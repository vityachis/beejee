<?php

namespace app\models;

use app\core\classes\Application;
use app\core\classes\DBWorker;
use app\core\classes\interfaces\IdentityInterface;
use app\core\classes\Model;

/**
 * Class User
 *
 * @property int|null $id
 * @property string|null $username
 * @property string|null $password
 * @property string|null $email
 * @property int|null $is_admin
 */
class User extends DBWorker implements IdentityInterface
{
    /**
     * User constructor.
     *
     * @param bool $initFieldNames
     * @param string $nameTable
     */
    public function __construct(bool $initFieldNames = false, string $nameTable = 'user')
    {
        parent::__construct($nameTable, $initFieldNames);
    }

    /**
     * @return $this|\app\core\classes\Model|null
     */
    public function getIdentityModel(): ?Model
    {
        if (empty($this->id)) {
            return null;
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    public function auth(string $username, string $password): bool
    {
        $user = $this->findOne('username', $username);
        if ($user !== null && password_verify($password . Application::getConfig('password.salt'), $user->password)) {
            $_SESSION['id'] = $user->id;

            return true;
        }

        return false;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function signup(string $username, string $email, string $password): bool
    {
        $result = $this->add([
            'username' => $username,
            'email'    => $email,
            'password' => password_hash($password . Application::getConfig('password.salt'), PASSWORD_BCRYPT),
        ]);

        if ($result) {
            return $this->auth($username, $password);
        }

        return false;
    }

    /**
     * @param int $id
     *
     * @return IdentityInterface|null
     */
    public function loginById(int $id): ?IdentityInterface
    {
        return $this->findOne('id', $id);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return (bool)$this->is_admin;
    }
}
