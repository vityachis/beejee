<?php

use app\core\classes\Application;

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR . '..');
define('CORE', __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'core');

require(ROOT . '/vendor/autoload.php');
ini_set('display_errors', (int)Application::getConfig('debug'));

(new Application())->run();
