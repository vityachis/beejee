<?php

use app\core\classes\AlertHelper;
use app\core\classes\Application;

/**
 * @var string $template
 * @var string $urlTemplate
 * @var array $content
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" href="/favicon.ico">

    <title><?= $content['title'] ?? '' ?></title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><?= Application::getConfig('main.nameProject') ?></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= $urlTemplate === 'task/index' ? 'active' : '' ?>"><a href="/task/index">Task grid</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (Application::identityUser()->getId()): ?>
                    <li>
                        <a href="/default/logout">Log Out (<?= Application::identityUser()->getUsername() ?>)</a>
                    </li>
                <?php else: ?>
                    <li class="<?= $urlTemplate === 'default/login' ? 'active' : '' ?>"><a href="/default/login">Log In</a></li>
                    <li class="<?= $urlTemplate === 'default/signup' ? 'active' : '' ?>"><a href="/default/signup">Sign Up</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php foreach (AlertHelper::getAlerts() as $alertMessage): ?>
                <div class="text-center alert alert-<?= $alertMessage['type'] ?>">
                    <strong><?= $alertMessage['name'] ?>!</strong> <?= $alertMessage['description'] ?>
                </div>
            <?php endforeach ?>

            <?php include($template); ?>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="/js/script.js"></script>
</body>
</html>
