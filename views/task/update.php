<?php
/**
 * @var array $content
 */
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <span><?= $content['title'] ?></span>
            <a href="/task/mark-to-complete?id=<?= $content['task']->id  ?>" class="btn btn-warning pull-right">Mark to complete</a>
        </h1>
    </div>
    <div class="col-md-12">
        <form method="post" action="/task/update?id=<?= $content['task']->id  ?>">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" placeholder="Username" required="required" disabled="disabled" value="<?= $content['task']->username ?>" maxlength="25" minlength="3">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="required" disabled="disabled" value="<?= $content['task']->email ?>" maxlength="50" minlength="6">
            </div>
            <div class="form-group">
                <label for="text">Text</label>
                <textarea name="text" class="form-control" id="text" rows="3" placeholder="..." required="required" maxlength="65535" minlength="4"><?= $content['task']->text ?></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default btn-center">Update</button>
            </div>
        </form>
    </div>
</div>
