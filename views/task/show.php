<?php

use app\core\classes\Application;
use app\models\Task;
use app\models\User;

/**
 * @var array $content
 * @var Task $task
 * @var User $user
 */

$task = $content['task'];
$user = $content['user'];
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <span><?= $content['title'] ?> [ <?= $task->getStatus() ?> ]</span>
            <?php if (!empty($content['task']) && Application::identityUser()->isAdmin()): ?>
                <a href="/task/update?id=<?= $content['task']->id ?>" class="btn btn-primary pull-right">Update task</a>
            <?php endif; ?>
        </h1>
    </div>
    <?php if (empty($content['task'])): ?>
        <div class="col-md-12">
            <div class="alert alert-danger text-center">Task not found.</div>
        </div>
    <?php else: ?>
        <div class="col-md-12">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="col">ID</th>
                    <td><?= $task->id ?></td>
                </tr>
                <tr>
                    <th scope="col">User ID</th>
                    <td><?= $task->user_id ?? '(not set)' ?></td>
                </tr>
                <tr>
                    <th scope="col">Username</th>
                    <td><?= $task->username ?></td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td><?= $task->email ?></td>
                </tr>
                <tr>
                    <th scope="col">Text</th>
                    <td><?= $task->text ?></td>
                </tr>
                <?php if ($task->update_user_id !== null): ?>
                    <tr>
                        <th scope="col">Updated by</th>
                        <td><?= $user->username ?? "#{$task->update_user_id}" ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
</div>
