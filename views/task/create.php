<?php
/**
 * @var array $content
 */

use app\core\classes\Application;

?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header"><?= $content['title'] ?></h1>
    </div>
    <div class="col-md-12">
        <form method="post" action="/task/create">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" placeholder="Username" required="required" value="<?= Application::identityUser()->getUsername() ?>" maxlength="25" minlength="3">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="required" value="<?= Application::identityUser()->getEmail() ?>" maxlength="50" minlength="6">
            </div>
            <div class="form-group">
                <label for="text">Text</label>
                <textarea name="text" class="form-control" id="text" rows="3" placeholder="..." required="required" maxlength="65535" minlength="4"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default btn-center">Create</button>
            </div>
        </form>
    </div>
</div>
