<?php
/**
 * @var array $content
 */
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <span><?= $content['title'] ?></span> <a href="/task/create" class="btn btn-primary pull-right">Create task</a>
        </h1>
    </div>
    <?php if (empty($content['tasks'])): ?>
        <div class="col-md-12">
            <div class="alert alert-danger text-center">Tasks not found.</div>
        </div>
    <?php else: ?>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        <span>ID</span>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => 'id']) ?>"><i class="fas fa-sort-numeric-down"></i></a>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => '-id']) ?>"><i class="fas fa-sort-numeric-up"></i></a>
                    </th>
                    <th>
                        <span>Username</span>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => 'username']) ?>"><i class="fas fa-sort-alpha-down"></i></a>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => '-username']) ?>"><i class="fas fa-sort-alpha-up"></i></a>
                    </th>
                    <th>
                        <span>Email</span>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => 'email']) ?>"><i class="fas fa-sort-alpha-down"></i></a>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => '-email']) ?>"><i class="fas fa-sort-alpha-up"></i></a>
                    </th>
                    <th colspan="2">
                        <span>Status</span>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => 'status']) ?>"><i class="fas fa-sort-amount-down"></i></a>
                        <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'], 'sort' => '-status']) ?>"><i class="fas fa-sort-amount-up"></i></a>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($content['tasks'] as $task): ?>
                    <tr>
                        <th scope="row">
                            <a href="/task/show?id=<?= $task->id ?>"><?= $task->id ?> <i class="fas fa-link"></i></a>
                        </th>
                        <td><?= $task->username ?></td>
                        <td><?= $task->email ?></td>
                        <td><?= $task->getStatus() ?></td>
                        <td><?= empty($task->update_user_id) ? '' : 'Updated by ' . $task->getUpdatedBy() ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <nav aria-label="Page navigation" class="pull-right">
                <ul class="pagination">
                    <?php if ($content['currentPage'] > 1): ?>
                        <li>
                            <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'] - 1, 'sort' => $content['sort']]) ?>" aria-label="Previous"><?= $content['currentPage'] - 1 ?></a>
                        </li>
                    <?php endif; ?>
                    <li class="disabled">
                        <a href="#" aria-label="Current"><?= $content['currentPage'] ?></a>
                    </li>
                    <?php if ($content['currentPage'] < $content['pageCount']): ?>
                        <li>
                            <a href="/task/index?<?= http_build_query(['page' => $content['currentPage'] + 1, 'sort' => $content['sort']]) ?>" aria-label="Next"><?= $content['currentPage'] + 1 ?></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    <?php endif; ?>
</div>
