<?php
/**
 * @var array $content
 */
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header text-center"><?= $content['title'] ?></h1>
    </div>
    <div class="col-md-6 col-md-offset-3">
        <form method="post" action="/default/signup">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" placeholder="Username" name="username" required="required" maxlength="25" minlength="3">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Email" name="email" required="required" maxlength="50" minlength="6">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Password" name="password" required="required" maxlength="16" minlength="3">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-default btn-center">Sign Up</button>
            </div>
        </form>
    </div>
</div>
