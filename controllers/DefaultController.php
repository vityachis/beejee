<?php

namespace app\controllers;

use app\core\classes\AlertHelper;
use app\core\classes\Controller;
use app\models\User;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * Login action
     */
    public function actionLogin(): void
    {
        if (!empty($_POST)) {
            if (empty($_POST['username']) || empty($_POST['password'])) {
                AlertHelper::setAlerts([
                    [
                        'type'        => 'danger',
                        'name'        => 'Error',
                        'description' => 'The username or password field is empty.',
                    ],
                ]);
                $this->view->generate($this->actionPath, [
                    'title' => 'Log In',
                ]);

                return;
            }

            $username    = trim($_POST['username']);
            $usernameLen = strlen($username);

            $password    = trim($_POST['password']);
            $passwordLen = strlen($password);

            $errors = [];

            if ($usernameLen < 3 || $usernameLen > 25) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Username must be more than 3 and less than 25 characters.',
                ];
            }

            if ($passwordLen < 3 || $passwordLen > 16) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Password must be more than 3 and less than 16 characters.',
                ];
            }

            if (!empty($errors)) {
                AlertHelper::setAlerts($errors);
                $this->view->generate($this->actionPath, [
                    'title' => 'Log In',
                ]);

                return;
            }

            if ((new User())->auth($username, $password)) {
                header('Location: /');

                return;
            }

            AlertHelper::setAlerts([
                [
                    'type'        => 'danger',
                    'name'        => 'Error',
                    'description' => 'Username or Password are incorrect.',
                ],
            ]);
            $this->view->generate($this->actionPath, [
                'title' => 'Log In',
            ]);

            return;
        }

        $this->view->generate($this->actionPath, [
            'title' => 'Log In',
        ]);
    }

    /**
     * Signup action
     */
    public function actionSignup(): void
    {
        if (!empty($_POST)) {
            if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password'])) {
                AlertHelper::setAlerts([
                    [
                        'type'        => 'danger',
                        'name'        => 'Error',
                        'description' => 'The username or email or password field is empty.',
                    ],
                ]);
                $this->view->generate($this->actionPath, [
                    'title' => 'Sign Up',
                ]);

                return;
            }

            $username    = trim($_POST['username']);
            $usernameLen = strlen($username);

            $email    = trim($_POST['email']);
            $emailLen = strlen($email);

            $password    = trim($_POST['password']);
            $passwordLen = strlen($password);

            $errors = [];

            if ($usernameLen < 3 || $usernameLen > 25) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Username must be more than 3 and less than 25 characters.',
                ];
            }

            if ($emailLen < 6 || $emailLen > 50) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Email must be more than 6 and less than 50 characters.',
                ];
            }

            if (preg_match('/.+@.+\..+/i', $email) === 0) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Email is not a valid email address.',
                ];
            }

            if ($passwordLen < 3 || $passwordLen > 16) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Password must be more than 3 and less than 16 characters.',
                ];
            }

            if (!empty($errors)) {
                AlertHelper::setAlerts($errors);
                $this->view->generate($this->actionPath, [
                    'title' => 'Sign Up',
                ]);

                return;
            }

            if ((new User())->signup($username, $email, $password)) {
                header('Location: /');

                return;
            }

            AlertHelper::setAlerts([
                [
                    'type'        => 'danger',
                    'name'        => 'Error',
                    'description' => 'Registration failed.',
                ],
            ]);
            $this->view->generate($this->actionPath, [
                'title' => 'Sign Up',
            ]);

            return;
        }

        $this->view->generate($this->actionPath, [
            'title' => 'Sign Up',
        ]);
    }

    /**
     * Logout action
     */
    public function actionLogout(): void
    {
        unset($_SESSION['id']);
        header('Location: /');
    }
}
