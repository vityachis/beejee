<?php

namespace app\controllers;

use app\core\classes\AlertHelper;
use app\core\classes\Application;
use app\core\classes\Controller;
use app\core\classes\DBWorker;
use app\core\classes\Route;
use app\models\Task;
use app\models\User;

/**
 * Class TaskController
 */
class TaskController extends Controller
{
    /**
     * Index action
     */
    public function actionIndex(): void
    {
        $sortParam = $this->getDataParam('sort', 'id');
        $sortField = preg_replace('/[\W]/', '', $sortParam);
        $sortOrder = strpos($sortParam, '-') === 0 ? DBWorker::SORT_DESC : DBWorker::SORT_ASC;

        $page = (int)$this->getDataParam('page', 1);
        if ($page <= 0) {
            $page = 1;
        }

        $offset = ($page - 1) * Task::COUNT_ON_PAGE;
        $length = Task::COUNT_ON_PAGE;

        $tasks     = (new Task)->findAllCondition([], $sortField, $sortOrder, "{$offset}, {$length}");
        $allTasks  = (new Task)->findAll();
        $pageCount = (int)floor(count($allTasks) / Task::COUNT_ON_PAGE);
        if (count($allTasks) % Task::COUNT_ON_PAGE > 0) {
            $pageCount++;
        }

        $this->view->generate($this->actionPath, [
            'title'       => 'Task grid',
            'tasks'       => $tasks,
            'pageCount'   => $pageCount,
            'currentPage' => $page,
            'sort'        => $sortParam,
        ]);
    }

    /**
     * Show action
     */
    public function actionShow(): void
    {
        $id   = $this->getDataParam('id');
        $task = (new Task())->findOne('id', $id);

        if ($task === null) {
            Route::errorPage404(Application::getConfig('main.defaultLayout'));
        }

        $user = (new User())->findOne('id', $task->update_user_id);
        
        $this->view->generate($this->actionPath, [
            'title' => "Task #{$id}",
            'task'  => $task ?: [],
            'user'  => $user,
        ]);
    }

    /**
     * Create action
     */
    public function actionCreate(): void
    {
        if (!empty($_POST)) {
            if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['text'])) {
                AlertHelper::setAlerts([
                    [
                        'type'        => 'danger',
                        'name'        => 'Error',
                        'description' => 'The username or email or text field is empty.',
                    ],
                ]);
                $this->view->generate($this->actionPath, [
                    'title' => 'Create task',
                ]);

                return;
            }

            $username    = trim($_POST['username']);
            $usernameLen = strlen($username);

            $email    = trim($_POST['email']);
            $emailLen = strlen($email);

            $text    = trim($_POST['text']);
            $textLen = strlen($text);

            $errors = [];

            if ($usernameLen < 3 || $usernameLen > 25) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Username must be more than 3 and less than 25 characters.',
                ];
            }

            if ($emailLen < 6 || $emailLen > 50) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Email must be more than 6 and less than 50 characters.',
                ];
            }

            if (preg_match('/.+@.+\..+/i', $email) === 0) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Email is not a valid email address.',
                ];
            }

            if ($textLen < 4 || $textLen > 65535) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Text must be more than 4 and less than 65535 characters.',
                ];
            }

            if (!empty($errors)) {
                AlertHelper::setAlerts($errors);
                $this->view->generate($this->actionPath, [
                    'title' => 'Create task',
                ]);

                return;
            }

            $taskData = [
                'username' => $username,
                'email'    => $email,
                'text'     => $text,
            ];
            if (Application::identityUser()->getId() !== null) {
                $taskData['user_id'] = Application::identityUser()->getId();
            }

            if ((new Task)->add($taskData)) {
                AlertHelper::setAlerts([
                    [
                        'type'        => 'success',
                        'name'        => 'Info',
                        'description' => 'Task successfully created.',
                    ],
                ]);
                header('Location: /');

                return;
            }

            AlertHelper::setAlerts([
                [
                    'type'        => 'danger',
                    'name'        => 'Error',
                    'description' => 'Create task failed.',
                ],
            ]);

            AlertHelper::setAlerts($errors);
            $this->view->generate($this->actionPath, [
                'title' => 'Create task',
            ]);

            return;
        }

        $this->view->generate($this->actionPath, [
            'title' => 'Create task',
        ]);
    }

    /**
     * Update action
     */
    public function actionUpdate(): void
    {
        $id   = $this->getDataParam('id');
        $task = (new Task())->findOne('id', $id);

        if (!Application::identityUser()->getId()) {
            AlertHelper::setAlerts([
                [
                    'type'        => 'info',
                    'name'        => 'Info',
                    'description' => 'You must be logged in.',
                ],
            ]);
            header('Location: /default/login');

            return;
        }

        if (!Application::identityUserIsAdmin()) {
            AlertHelper::setAlerts([
                [
                    'type'        => 'info',
                    'name'        => 'Permission denied',
                    'description' => 'You do not have permission to edit the task.',
                ],
            ]);
            header("Location: /task/show?id={$id}");

            return;
        }

        if ($task === null) {
            Route::errorPage404(Application::getConfig('main.defaultLayout'));
        }

        if (!empty($_POST)) {
            if (empty($_POST['text'])) {
                AlertHelper::setAlerts([
                    [
                        'type'        => 'danger',
                        'name'        => 'Error',
                        'description' => 'Text field cannot be empty.',
                    ],
                ]);
                $this->view->generate($this->actionPath, [
                    'title' => 'Update task',
                ]);

                return;
            }

            $text    = trim($_POST['text']);
            $textLen = strlen($text);
            $errors  = [];

            if ($textLen < 4 || $textLen > 65535) {
                $errors[] = [
                    'type'        => 'danger',
                    'name'        => 'Validation error',
                    'description' => 'Text must be more than 4 and less than 65535 characters.',
                ];
            }

            if (!empty($errors)) {
                AlertHelper::setAlerts($errors);
                $this->view->generate($this->actionPath, [
                    'title' => 'Create task',
                ]);

                return;
            }

            if ((new Task)->update('id', $id, [
                'text'           => $text,
                'update_user_id' => Application::identityUser()->getId(),
            ])) {
                AlertHelper::setAlerts([
                    [
                        'type'        => 'success',
                        'name'        => 'Info',
                        'description' => 'Task successfully updated.',
                    ],
                ]);
                header("Location: /task/show?id={$id}");

                return;
            }
        }

        $this->view->generate($this->actionPath, [
            'title' => 'Update task',
            'task'  => $task,
        ]);
    }

    /**
     * Mark to complete action
     */
    public function actionMarkToComplete(): void
    {
        $id   = $this->getDataParam('id');
        $task = (new Task())->findOne('id', $id);

        if (!Application::identityUser()->getId()) {
            AlertHelper::setAlerts([
                [
                    'type'        => 'info',
                    'name'        => 'Info',
                    'description' => 'You must be logged in.',
                ],
            ]);
            header('Location: /default/login');

            return;
        }

        if (!Application::identityUserIsAdmin()) {
            AlertHelper::setAlerts([
                [
                    'type'        => 'info',
                    'name'        => 'Permission denied',
                    'description' => 'You do not have permission to edit the task.',
                ],
            ]);
            header("Location: /task/show?id={$id}");

            return;
        }

        if ($task === null) {
            Route::errorPage404(Application::getConfig('main.defaultLayout'));
        }

        (new Task)->update('id', $id, [
            'status' => Task::STATUS_COMPLETED,
        ]);

        header("Location: /task/show?id={$id}");
    }
}
