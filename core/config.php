<?php

return [
    'debug'                  => 0,
    'password.salt'          => '',
    'identity.model'         => null,
    'main.defaultController' => 'default',
    'main.defaultAction'     => 'index',
    'main.defaultLayout'     => 'main',
    'main.errorPage404'      => 'error-page-404',
    'main.nameProject'       => 'vityachis',
];
