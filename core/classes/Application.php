<?php

namespace app\core\classes;

use app\core\classes\interfaces\IdentityInterface;
use http\Exception\RuntimeException;
use PDO;
use PDOException;

/**
 * Class Application
 */
class Application
{
    /**
     * @var string
     */
    private $layout;

    /**
     * @var PDO
     */
    private static $dbConnect;

    /**
     * @var array
     */
    private static $systemConfig;

    /**
     * @var IdentityInterface|null
     */
    private static $identityUser;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->layout = strtolower(self::getConfig('main.defaultLayout'));
    }

    /**
     * Run application
     */
    public function run(): void
    {
        session_start();

        self::identityUser();

        $route = new Route();
        $route->route();

        if (empty(self::$dbConnect)) {
            self::connectDB();
        }

        $controllerName = "\\app\\controllers\\" . $route->getControllerName();
        if (file_exists(ROOT . '/controllers/' . $route->getControllerName() . '.php')) {
            $controller = new $controllerName($route->getActionPath(), $this->layout, $route->getDataParams());

            if (method_exists($controller, $route->getActionName())) {
                $controller->{$route->getActionName()}();
            } else {
                Route::errorPage404($this->layout);
            }
        } else {
            Route::errorPage404($this->layout);
        }
    }

    /**
     * @return PDO
     */
    public static function getDbConnect(): PDO
    {
        if (empty(self::$dbConnect)) {
            self::connectDB();
        }

        return self::$dbConnect;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function getConfig(string $key)
    {
        if (empty(self::$systemConfig)) {
            self::$systemConfig = array_merge(
                require(CORE . '/config.php'),
                require(ROOT . '/config.php')
            );
        }

        return self::$systemConfig[$key] ?? null;
    }

    /**
     * Connected database
     */
    protected static function connectDB(): void
    {
        $dbConfig = self::getConfig('database');
        try {
            self::$dbConnect = new PDO($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password']);
            if (self::getConfig('debug') === 1) {
                self::$dbConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        } catch (PDOException $e) {
            die('PDO Error: ' . $e->getMessage());
        }
    }

    /**
     * @return \app\core\classes\interfaces\IdentityInterface
     */
    public static function identityUser(): IdentityInterface
    {
        if (empty(self::$identityUser)) {
            $identityModel = self::getConfig('identity.model');
            if (!class_exists($identityModel)) {
                throw new RuntimeException('The "identity.model" parameter must be specified.');
            }

            self::$identityUser = (new $identityModel(true));
            if (isset($_SESSION['id'])) {
                self::$identityUser = self::$identityUser->loginById($_SESSION['id']) ?? self::$identityUser;
            }
        }

        return self::$identityUser;
    }

    /**
     * @return bool
     */
    public static function identityUserIsAdmin(): bool
    {
        return isset(self::$identityUser) && self::$identityUser->isAdmin();
    }
}
