<?php

namespace app\core\classes;

use app\core\classes\interfaces\ControllerInterface;

/**
 * Class Controller
 */
class Controller implements ControllerInterface
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var string
     */
    protected $actionPath;

    /**
     * @var array
     */
    private $dataParams;

    /**
     * Controller constructor.
     *
     * @param string $actionPath
     * @param string $layout
     * @param array $dataParams
     */
    public function __construct(string $actionPath, string $layout, array $dataParams = [])
    {
        $this->view       = new View($layout);
        $this->actionPath = $actionPath;
        $this->dataParams = $dataParams;
    }

    /**
     * @param string $key
     * @param mixed $defaultValue
     *
     * @return mixed
     */
    public function getDataParam(string $key, $defaultValue = null)
    {
        return $this->dataParams[$key] ?? $defaultValue;
    }

    /**
     * @return array
     */
    public function getDataParams(): array
    {
        return $this->dataParams;
    }
}
