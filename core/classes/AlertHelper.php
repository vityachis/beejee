<?php

namespace app\core\classes;

/**
 * Class AlertHelper
 */
class AlertHelper
{
    /**
     * @param array $alerts
     */
    public static function setAlerts(array $alerts): void
    {
        $_SESSION['alerts'] = $alerts;
    }

    /**
     * @param array $alert
     */
    public static function addAlert(array $alert): void
    {
        if (empty($_SESSION['alerts'])) {
            $_SESSION['alerts'] = [];
        }
        $_SESSION['alerts'][] = $alert;
    }

    /**
     * @return array
     */
    public static function getAlerts(): array
    {
        $alerts = $_SESSION['alerts'] ?? [];
        unset($_SESSION['alerts']);

        return $alerts;
    }
}
