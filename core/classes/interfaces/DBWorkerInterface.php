<?php

namespace app\core\classes\interfaces;

/**
 * Interface DBWorkerInterface
 */
interface DBWorkerInterface
{
    public const SORT_DESC = 'DESC';
    public const SORT_ASC  = 'ASC';

    /**
     * @param string $sortField
     * @param string $sortOrder
     * @param string $limit
     *
     * @return array
     */
    public function findAll(string $sortField = 'id', string $sortOrder = self::SORT_ASC, string $limit = '25'): array;

    /**
     * @param array $condition
     * @param string $sortField
     * @param string $sortOrder
     * @param string $limit
     *
     * @return array
     */
    public function findAllCondition(array $condition, string $sortField = 'id', string $sortOrder = self::SORT_ASC, string $limit = '25'): array;

    /**
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    public function findOne(string $field, $value);

    /**
     * @param array $data
     *
     * @return bool
     */
    public function add(array $data): bool;

    /**
     * @param string $field
     * @param mixed $value
     * @param array $data
     *
     * @return bool
     */
    public function update(string $field, $value, array $data = []): bool;

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return bool
     */
    public function delete(string $field, $value): bool;

    /**
     * @param string $name
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function __get(string $name);

    /**
     * @param string $name
     * @param mixed $value
     *
     * @throws \ReflectionException
     */
    public function __set(string $name, $value): void;

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset(string $name): bool;
}
