<?php

namespace app\core\classes\interfaces;

/**
 * Interface IdentityInterface
 */
interface IdentityInterface
{
    /**
     * IdentityInterface constructor.
     *
     * @param bool $initFieldNames
     * @param string $nameTable
     */
    public function __construct(bool $initFieldNames, string $nameTable);

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string|null
     */
    public function getUsername(): ?string;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    public function auth(string $username, string $password): bool;

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function signup(string $username, string $email, string $password): bool;

    /**
     * @param int $id
     *
     * @return IdentityInterface|null
     */
    public function loginById(int $id): ?IdentityInterface;

    /**
     * @return bool
     */
    public function isAdmin(): bool;
}
