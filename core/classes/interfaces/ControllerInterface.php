<?php

namespace app\core\classes\interfaces;

/**
 * Interface ControllerInterface
 */
interface ControllerInterface
{
    /**
     * @param string $key
     * @param mixed $defaultValue
     *
     * @return mixed
     */
    public function getDataParam(string $key, $defaultValue = null);

    /**
     * @return array
     */
    public function getDataParams(): array;
}
