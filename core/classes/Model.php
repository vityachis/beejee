<?php

namespace app\core\classes;

/**
 * Class Model
 */
class Model
{
    /**
     * @var string
     */
    private $nameTable;

    /**
     * Model constructor.
     *
     * @param string $nameTable
     */
    public function __construct(string $nameTable)
    {
        $this->nameTable = $nameTable;
    }

    /**
     * @return string
     */
    public function getNameTable(): string
    {
        return $this->nameTable;
    }
}
