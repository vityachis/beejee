<?php

namespace app\core\classes;

/**
 * Class Route
 */
class Route
{
    /**
     * @var string
     */
    private $controllerName;

    /**
     * @var string
     */
    private $actionName;

    /**
     * @var string
     */
    private $fullActionPath;

    /**
     * @var string
     */
    private $actionPathFolder;

    /**
     * @var string
     */
    private $actionPathFile;

    /**
     * @var array
     */
    private $dataParams = [];

    /**
     * Route constructor.
     */
    public function __construct()
    {
        $this->controllerName = self::transformUrl(Application::getConfig('main.defaultController')) . 'Controller';

        $this->actionPathFolder = strtolower(Application::getConfig('main.defaultController'));
        $this->actionPathFile   = strtolower(Application::getConfig('main.defaultAction'));
        $this->actionName       = 'action' . self::transformUrl(Application::getConfig('main.defaultAction'));
    }

    /**
     * Run route
     */
    public function route(): void
    {
        $url = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($url[1])) {
            $this->controllerName   = self::transformUrl($url[1]) . 'Controller';
            $this->actionPathFolder = $url[1];
        }

        if (!empty($url[2])) {
            $urlParam             = explode('?', $url[2]);
            $this->actionPathFile = $urlParam[0];

            if (count($urlParam) >= 2) {
                $getData = explode('&', $urlParam[1]);

                $dataParams = [];

                foreach ($getData as $item) {
                    $val = explode('=', $item);
                    if (!isset($val[1])) {
                        continue;
                    }

                    $dataParams[$val[0]] = $val[1];
                }
                $this->dataParams = $dataParams;
            }

            $this->actionName = 'action' . self::transformUrl($this->actionPathFile);
        }

        $this->fullActionPath = $this->actionPathFolder . '/' . $this->actionPathFile;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public static function transformUrl(string $url): string
    {
        $transform = explode('-', $url);
        $result    = '';
        foreach ($transform as $partUrl) {
            $result .= ucfirst($partUrl);
        }

        return $result;
    }

    /**
     * @param string $layout
     */
    public static function errorPage404(string $layout): void
    {
        header("HTTP/1.0 404 Not Found");
        (new View($layout))->generate(Application::getConfig('main.errorPage404'), ['title' => '404: Not Found']);
        die();
    }

    /**
     * @return string
     */
    public function getControllerName(): string
    {
        return $this->controllerName;
    }

    /**
     * @return string
     */
    public function getActionName(): string
    {
        return $this->actionName;
    }

    /**
     * @return string
     */
    public function getActionPath(): string
    {
        return $this->fullActionPath;
    }

    /**
     * @return array
     */
    public function getDataParams(): array
    {
        return $this->dataParams;
    }
}
