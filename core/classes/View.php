<?php

namespace app\core\classes;

/**
 * Class View
 */
class View
{
    /**
     * @var string
     */
    private $layout;

    /**
     * View constructor.
     *
     * @param string $layout
     */
    public function __construct(string $layout)
    {
        $this->layout = $layout;
    }

    /**
     * @param string $urlTemplate
     * @param array $content
     */
    public function generate(string $urlTemplate, array $content = []): void
    {
        if (empty($content['title'])) {
            $content['title'] = $urlTemplate;
        }

        if (file_exists(ROOT . "/views/{$urlTemplate}.php")) {
            $template = ROOT . "/views/{$urlTemplate}.php";
        } else {
            $template = CORE . "/views/{$urlTemplate}.php";
        }

        if (file_exists(ROOT . "/views/layouts/{$this->layout}.php")) {
            $layout = ROOT . "/views/layouts/{$this->layout}.php";
        } else {
            $layout = CORE . '/views/layout.php';
        }

        include($layout);
    }
}
