<?php

namespace app\core\classes;

use app\core\classes\interfaces\DBWorkerInterface;
use PDO;
use ReflectionException;

/**
 * Class DBWorker
 */
class DBWorker extends Model implements DBWorkerInterface
{
    /**
     * @var array
     */
    protected $fields;

    /**
     * @var PDO
     */
    private $dbConnect;

    /**
     * ActiveRecord constructor.
     *
     * @param string $nameTable
     * @param bool $initFieldNames
     */
    public function __construct(string $nameTable, bool $initFieldNames = false)
    {
        $this->dbConnect = Application::getDbConnect();
        if ($initFieldNames) {
            $fields   = [];
            $fetchAll = $this->dbConnect->query('SHOW COLUMNS FROM ' . $nameTable)->fetchAll();
            if (is_array($fetchAll)) {
                foreach ($fetchAll as $row) {
                    $fields[$row['Field']] = null;
                }
            }
            $this->setFields($fields);
        }

        parent::__construct($nameTable);
    }

    /**
     * @param string $sortField
     * @param string $sortOrder
     * @param string $limit
     *
     * @return array
     */
    public function findAll(string $sortField = 'id', string $sortOrder = self::SORT_ASC, string $limit = '0, 25'): array
    {
        $allRecords = $this->dbConnect->query('SELECT * FROM ' . $this->getNameTable() . ' ORDER BY ' . $sortField . ' ' . $sortOrder . ' LIMIT ' . $limit)->fetchAll(PDO::FETCH_ASSOC);
        $result     = [];

        foreach ($allRecords as $fields) {
            $result[] = (new static($this->getNameTable()))->setFields($fields);
        }

        return $result;
    }

    /**
     * @param array $condition
     * @param string $sortField
     * @param string $sortOrder
     * @param string $limit
     *
     * @return array
     */
    public function findAllCondition(array $condition, string $sortField = 'id', string $sortOrder = self::SORT_ASC, string $limit = '0, 25'): array
    {
        if (empty($condition)) {
            return $this->findAll($sortField, $sortOrder, $limit);
        }

        $params         = '';
        $firstCondition = true;
        foreach ($condition as $field => $value) {
            $fieldFormatted = preg_replace('/[\W]/', '', $field);
            if ($firstCondition) {
                $firstCondition = false;
                $params         .= $fieldFormatted . ' = :' . $fieldFormatted;
            } else {
                $params .= ' AND ' . $fieldFormatted . ' = :' . $fieldFormatted;
            }
        }

        $pdoStatement = $this->dbConnect->prepare('SELECT * FROM ' . $this->getNameTable() . ' WHERE ' . $params . ' ORDER BY ' . $sortField . ' ' . $sortOrder . ' LIMIT ' . $limit);
        foreach ($condition as $field => $value) {
            $fieldFormatted = preg_replace('/[\W]/', '', $field);
            $pdoStatement->bindValue(':' . $fieldFormatted, $value);
        }

        $pdoStatement->execute();

        $allRecords = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        $result     = [];

        foreach ($allRecords as $fields) {
            $result[] = (new static($this->getNameTable()))->setFields($fields);
        }

        return $result;
    }

    /**
     * @param string $field
     * @param string $value
     *
     * @return static|null
     */
    public function findOne(string $field, $value): ?DBWorker
    {
        $fieldFormatted = preg_replace('/[\W]/', '', $field);
        $pdoStatement   = $this->dbConnect->prepare('SELECT * FROM ' . $this->getNameTable() . ' WHERE ' . $fieldFormatted . ' = :value');
        $pdoStatement->bindValue(':value', $value);
        $pdoStatement->execute();

        $fields = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if (is_array($fields)) {
            return (new static($this->getNameTable()))->setFields($fields);
        }

        return null;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function add(array $data): bool
    {
        $fields     = '';
        $values     = '';
        $firstParam = true;

        foreach ($data as $field => $value) {
            $fieldFormatted = preg_replace('/[\W]/', '', $field);
            if ($firstParam) {
                $firstParam = false;
                $fields     .= $fieldFormatted;
                $values     .= ':' . $fieldFormatted;
            } else {
                $fields .= ', ' . $fieldFormatted;
                $values .= ', :' . $fieldFormatted;
            }
        }

        $pdoStatement = $this->dbConnect->prepare('INSERT INTO ' . $this->getNameTable() . ' (' . $fields . ') VALUES (' . $values . ')');

        foreach ($data as $field => $value) {
            $encodeValue    = htmlspecialchars($value);
            $fieldFormatted = preg_replace('/[\W]/', '', $field);
            $pdoStatement->bindValue(':' . $fieldFormatted, $encodeValue);
        }

        return $pdoStatement->execute();
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param array $data
     *
     * @return bool
     */
    public function update(string $field, $value, array $data = []): bool
    {
        $params     = '';
        $firstParam = true;
        foreach ($data as $fieldData => $valueData) {
            $fieldDataFormatted = preg_replace('/[\W]/', '', $fieldData);
            if ($firstParam) {
                $firstParam = false;
                $params     .= $fieldDataFormatted . ' = :' . $fieldDataFormatted;
            } else {
                $params .= ', ' . $fieldDataFormatted . ' = :' . $fieldDataFormatted;
            }
        }

        $fieldQueryFormatted = preg_replace('/[\W]/', '', $field);
        $pdoStatement        = $this->dbConnect->prepare('UPDATE ' . $this->getNameTable() . ' SET ' . $params . ' WHERE ' . $fieldQueryFormatted . ' = :value');

        foreach ($data as $fieldData => $valueData) {
            $encodeValue        = htmlspecialchars($valueData);
            $fieldDataFormatted = preg_replace('/[\W]/', '', $fieldData);
            $pdoStatement->bindValue(':' . $fieldDataFormatted, $encodeValue);
        }
        $pdoStatement->bindValue(':value', $value);

        return $pdoStatement->execute();
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return bool
     */
    public function delete(string $field, $value): bool
    {
        $fieldFormatted = preg_replace('/[\W]/', '', $field);
        $pdoStatement   = $this->dbConnect->prepare('DELETE FROM ' . $this->getNameTable() . ' WHERE ' . $fieldFormatted . ' = :value LIMIT 1');

        $pdoStatement->bindValue(':value', $value);

        return $pdoStatement->execute();
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function __get(string $name)
    {
        $fields = $this->fields;

        if (array_key_exists($name, $fields)) {
            return $this->fields[$name];
        }

        throw new ReflectionException("Undefined property: {$name}.");
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @throws \ReflectionException
     */
    public function __set(string $name, $value): void
    {
        $fields = $this->fields;
        if (array_key_exists($name, $fields)) {
            $this->$fields[$name] = $value;

            return;
        }

        throw new ReflectionException("Undefined property: {$name}.");
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset(string $name): bool
    {
        return array_key_exists($name, $this->fields);
    }

    /**
     * @return array
     */
    protected function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     *
     * @return static
     */
    protected function setFields(array $fields): DBWorker
    {
        $this->fields = $fields;

        return $this;
    }
}
