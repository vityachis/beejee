<?php
/**
 * @var array $content
 */
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header text-center"><?= $content['title'] ?></h1>
    </div>
    <div class="col-md-12">
        <div class="alert alert-danger text-center" role="alert">404: Not Found</div>
    </div>
</div>
