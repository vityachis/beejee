<?php
/**
 * @var string $template
 * @var string $urlTemplate
 * @var array $content
 */

use app\core\classes\AlertHelper;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?= $content['title'] ?? '' ?></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php foreach (AlertHelper::getAlerts() as $alertMessage): ?>
                <div class="text-center alert alert-<?= $alertMessage['type'] ?>">
                    <strong><?= $alertMessage['name'] ?>!</strong> <?= $alertMessage['description'] ?>
                </div>
            <?php endforeach ?>

            <?php include($template); ?>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>
</html>
